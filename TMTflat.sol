pragma solidity ^0.4.21;

// File: contracts/ownership/Ownable.sol

/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable {
  address public owner;


  event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);


  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  constructor() public {
    owner = msg.sender;
  }

  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param newOwner The address to transfer ownership to.
   */
  function transferOwnership(address newOwner) public onlyOwner {
    require(newOwner != address(0));
    emit OwnershipTransferred(owner, newOwner);
    owner = newOwner;
  }

}

// File: contracts/math/SafeMath.sol

/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  function mul(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function div(uint256 a, uint256 b) internal pure returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }

  function sub(uint256 a, uint256 b) internal pure returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  function add(uint256 a, uint256 b) internal pure returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }
}

// File: contracts/token/ERC20Basic.sol

/**
 * @title ERC20Basic
 * @dev Simpler version of ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/179
 */
contract ERC20Basic {
  function totalSupply() public view returns (uint256);
  function balanceOf(address who) public view returns (uint256);
  function transfer(address to, uint256 value) public returns (bool);
  event Transfer(address indexed from, address indexed to, uint256 value);
}

// File: contracts/token/BasicToken.sol

/**
 * @title Basic token
 * @dev Basic version of StandardToken, with no allowances.
 */
contract BasicToken is ERC20Basic {
  using SafeMath for uint256;

  mapping(address => uint256) balances;

  uint256 totalSupply_;

  /**
  * @dev total number of tokens in existence
  */
  function totalSupply() public view returns (uint256) {
    return totalSupply_;
  }

  /**
  * @dev transfer token for a specified address
  * @param _to The address to transfer to.
  * @param _value The amount to be transferred.
  */
  function transfer(address _to, uint256 _value) public returns (bool) {
    require(_to != address(0));
    require(_value <= balances[msg.sender]);

    // SafeMath.sub will throw if there is not enough balance.
    balances[msg.sender] = balances[msg.sender].sub(_value);
    balances[_to] = balances[_to].add(_value);
    emit Transfer(msg.sender, _to, _value);
    return true;
  }

  /**
  * @dev Gets the balance of the specified address.
  * @param _owner The address to query the the balance of.
  * @return An uint256 representing the amount owned by the passed address.
  */
  function balanceOf(address _owner) public view returns (uint256 balance) {
    return balances[_owner];
  }

}

// File: contracts/token/MintableToken.sol

/**
 * @title Mintable token
 * @dev Simple ERC20 Token example, with mintable token creation
 * @dev Issue: * https://github.com/OpenZeppelin/zeppelin-solidity/issues/120
 * Based on code by TokenMarketNet: https://github.com/TokenMarketNet/ico/blob/master/contracts/MintableToken.sol
 */
contract MintableToken is BasicToken, Ownable {
  event Mint(address indexed to, uint256 amount);
  event MintFinished();

  bool public mintingFinished = false;


  modifier canMint() {
    require(!mintingFinished);
    _;
  }

  /**
   * @dev Function to mint tokens
   * @param _to The address that will receive the minted tokens.
   * @param _amount The amount of tokens to mint.
   * @return A boolean that indicates if the operation was successful.
   */
  function mint(address _to, uint256 _amount) onlyOwner canMint public returns (bool) {
    totalSupply_ = totalSupply_.add(_amount);
    balances[_to] = balances[_to].add(_amount);
    emit Mint(_to, _amount);
    emit Transfer(address(0), _to, _amount);
    return true;
  }

  /**
   * @dev Function to stop minting new tokens.
   * @return True if the operation was successful.
   */
  function finishMinting() onlyOwner canMint public returns (bool) {
    mintingFinished = true;
    emit MintFinished();
    return true;
  }
}

// File: contracts/token/PoolBidToken.sol

contract PoolBidToken is MintableToken {

    mapping (string => uint256) diamondType;
    mapping (uint256 => Order) public orders;
    uint256 public orderCounter;

    constructor() public {
        orderCounter = 0;
    }

    struct Order {
        uint256 id;
        uint256 target;
        uint256 currentTokenCount;
        uint expirationDate;
        uint256 tokenEthRatio;
        string metadata;
        mapping (address => uint256) investors;
    }

    modifier isStillOpen(uint256 orderID) {
        require(orders[orderID].expirationDate > now);
        require(orders[orderID].currentTokenCount > 0);
        _;
    }

    modifier isClosed(uint256 orderID) {
        require(orders[orderID].expirationDate <= now);
        require(orders[orderID].currentTokenCount > 0);
        _;
    }

    modifier isFilled(uint256 orderID) {
        require(orders[orderID].currentTokenCount == 0);
        _;
    }

    modifier canWithdraw(uint256 orderID) {
        require(orders[orderID].expirationDate > now + 6 hours);
        _;
    }

    function addType(string newType, uint256 targetTokens) onlyOwner public {
        diamondType[newType] = targetTokens;
    }

    function getTokenTarget(string theType) public view returns (uint256) {
        return diamondType[theType];
    }

    function createOrder(string theType, string metadata) onlyOwner public payable {
        // Assumption: msg.value has exactly the amount of ETH needed
        // TODO: ratio should be based on TMT and ether curent price
        Order memory order = Order(
            orderCounter,
            getTokenTarget(theType),
            getTokenTarget(theType),
            now + 1 days,
            msg.value.div(getTokenTarget(theType)),
            metadata
        );
        orders[orderCounter] = order;
        emit OrderCreated(orderCounter, theType, now + 1 days, msg.value.div(getTokenTarget(theType)), metadata, now);

        orderCounter += 1;
    }

    function bid(uint256 orderID, uint256 tokenAmount) isStillOpen(orderID) public {
        require(balances[msg.sender] >= tokenAmount);

        uint256 overbid;
        if(orders[orderID].currentTokenCount < tokenAmount) {
            overbid = tokenAmount - orders[orderID].currentTokenCount;
            tokenAmount = orders[orderID].currentTokenCount;
        }

        // Update smart contract state.
        balances[msg.sender] = balances[msg.sender].sub(tokenAmount);
        totalSupply_ = totalSupply_.sub(tokenAmount);

        // Update order state.
        orders[orderID].currentTokenCount = orders[orderID].currentTokenCount.sub(tokenAmount);
        orders[orderID].investors[msg.sender] = orders[orderID].investors[msg.sender].add(tokenAmount);
        if(orders[orderID].currentTokenCount == 0) {
            emit OrderIsComplete(orderID, msg.sender, now);
        }
        emit BidSubmitted(orderID, tokenAmount, overbid, msg.sender, now);
    }

    function withdraw(uint256 orderID, uint256 amount) isStillOpen(orderID) canWithdraw(orderID) public {
        require(orders[orderID].investors[msg.sender] >= amount);

        // Update smart contract state.
        balances[msg.sender] = balances[msg.sender].add(amount);
        totalSupply_ = totalSupply_.add(amount);

        // Update order state.
        orders[orderID].investors[msg.sender] = orders[orderID].investors[msg.sender].sub(amount);
        orders[orderID].currentTokenCount = orders[orderID].currentTokenCount.add(amount);
        emit BidWithdrawal(orderID, amount, msg.sender, now);
    }

    function claimOrderTokens(uint256 orderID) isClosed(orderID) public {
        require(orders[orderID].investors[msg.sender] > 0);
        uint256 amount = orders[orderID].investors[msg.sender];

        balances[msg.sender] = balances[msg.sender].add(orders[orderID].investors[msg.sender]);
        totalSupply_ = totalSupply_.add(orders[orderID].investors[msg.sender]);
        orders[orderID].investors[msg.sender] = 0;
        emit TokensClaimed(orderID, msg.sender, amount, now);
    }

    function claimOrderEther(uint256 orderID) isFilled(orderID) public {
        require(orders[orderID].investors[msg.sender] > 0);

        uint256 amount = orders[orderID].investors[msg.sender].mul(orders[orderID].tokenEthRatio);
        orders[orderID].investors[msg.sender] = 0;
        msg.sender.transfer(amount);
        emit EtherClaimed(orderID, msg.sender, amount, now);
    }

    event OrderCreated(uint256 orderCounter, string theType, uint256 expirationDate, uint256 conversion, string metadata, uint timestamp);
    event OrderIsComplete(uint256 orderID, address senderAddress, uint timestamp);
    event BidSubmitted(uint256 orderID, uint256 amount, uint256 overbid, address senderAddress, uint timestamp);
    event BidWithdrawal(uint256 orderID, uint256 amount, address senderAddress, uint timestamp);
    event TokensClaimed(uint256 orderID, address senderAddress, uint256 claimedTokens, uint timestamp);
    event EtherClaimed(uint256 orderID, address senderAddress, uint256 etherAmount, uint timestamp);
}

// File: contracts/token/TimeMachineToken.sol

contract TimeMachineToken is PoolBidToken {

    string public constant name = "LakeDiamond Token"; // solium-disable-line uppercase
    string public constant symbol = "LKD"; // solium-disable-line uppercase

    uint256 public constant INITIAL_SUPPLY = 141120000;

    event EtherReceived(address _sender, uint256 _amount);
    /**
    * @dev Constructor that gives msg.sender all of existing tokens.
    */
    constructor() public {
        totalSupply_ = INITIAL_SUPPLY;
        balances[msg.sender] = INITIAL_SUPPLY;
        emit Transfer(0x0, msg.sender, INITIAL_SUPPLY);
    }

    function buyTMT(uint256 amount) public returns (bool) {
        balances[owner] = balances[owner].sub(amount);
        balances[msg.sender] = balances[msg.sender].add(amount);
        emit Transfer(owner, msg.sender, amount);
        return true;
    }

    /**
    * Only owner can send ether to smart contract.
    */
    function () onlyOwner public payable {
        emit EtherReceived(msg.sender, msg.value);
    }

}
