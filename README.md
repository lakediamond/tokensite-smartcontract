# Tokensite Smart Contract

Contains:
- **TimeMachineToken** contract `contracts/token`: the TMT token description and features (ERC20, mintable, pausable)
- **(Not used) LakeDiamondCrowsale** contract `contracts/crowdsale`: managing the ICO sales (cap, dates, whitelisting)

## Latest deployment
- To deploy, use the documentation [here](https://gitlab.com/lakediamond/lkd-smartcontract/wikis/home)
- Active contract on Rinkeby: [0x03f8777c097a56698a51dbf274a362a4a503508c](https://rinkeby.etherscan.io/address/0x03f8777c097a56698a51dbf274a362a4a503508c#code) <-- deployed with LKD backend wallet

## Description
- The contract `TimeMachineToken.sol` with functions `Bids/new_order/mint-TMT` is [0x03f8777c097a56698a51dbf274a362a4a503508c](https://rinkeby.etherscan.io/address/0x03f8777c097a56698a51dbf274a362a4a503508c#code)
- (Server) LD Backend wallet + `TimeMachineToken.sol` contract owner [0xce27c70c04736bd7da7e23ffb579c88d00bdf3e6](https://rinkeby.etherscan.io/address/0xce27c70c04736bd7da7e23ffb579c88d00bdf3e6). Wallet creates orders (needs few (1 or 2) ETH for demos)
  - If empty, use [faucet](https://faucet.rinkeby.io) or [faucet](http://www.rinkebyfaucet.com/) 
- (Alice wallet) Metamask rinkeby [0x66459C02f4459cab5DBB77f487190D85C8299Baf](https://rinkeby.etherscan.io/address/0x66459C02f4459cab5DBB77f487190D85C8299Baf) alice@demo.com. Wallet used to make proposal on orders.
  - If wallet is empty, use (metamask) LD backend to send some LKD to alice.


