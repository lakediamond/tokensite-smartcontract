module.exports = function(error) {
    assert.isAbove(error.message.search('assert'), -1, 'Invalid opcode error must be returned, message:' + error.message);
}
