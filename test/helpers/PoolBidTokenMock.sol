pragma solidity ^0.4.21;

import "../../contracts/token/PoolBidToken.sol";

contract PoolBidTokenMock is PoolBidToken {

    function createMockOrder(string theType, string metadata, uint timestamp, uint256 tokenAmount) onlyOwner public payable {
        Order memory order = Order(
            orderCounter,
            getTokenTarget(theType),
            getTokenTarget(theType),
            timestamp,
            msg.value.div(getTokenTarget(theType)),
            metadata
        );
        orders[orderCounter] = order;
        orders[orderCounter].investors[msg.sender] = tokenAmount;
        emit OrderCreated(orderCounter, theType, timestamp, msg.value.div(getTokenTarget(theType)), metadata, now);
    }
}
