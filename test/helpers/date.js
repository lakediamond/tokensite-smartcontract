Date.prototype.addHours = function(hours) {
    this.setHours(this.getHours() + hours);
    return this;
}

Date.prototype.subHours = function(hours) {
    this.setHours(this.getHours() - hours);
    return this;
}
