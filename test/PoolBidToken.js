const checkRequire = require("./helpers/checkRequire.js");
const checkAssert = require("./helpers/checkAssert.js");
const date = require("./helpers/date.js");
var PoolBidToken = artifacts.require("../contracts/token/PoolBidToken.sol");
var PoolBidTokenMock = artifacts.require("./helpers/PoolBidTokenMock.sol");

contract('PoolBidToken', async (accounts) => {
    let token;

    beforeEach(async function() {
        token = await PoolBidToken.new();
    });

    it("should create a new diamond type", async () => {
        await token.addType("brilliant", 100);

        let target = await token.getTokenTarget("brilliant");
        assert.equal(target, 100);
    })

    it("should throw if a different address than the owner tries to create an order", async () => {
        await token.addType("platelets", 50);

        try {
            let createAnOrder = await token.createOrder("platelets", "metadata" , {from:accounts[1]});
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should throw if the type of the new order doesn't exist", async () => {

        try {
            createAnOrder = await token.createOrder("platelets", "metadata");
            assert.fail("should have thrown");
        } catch (error) {
            //Trhows inside order creation when it calculates the rate(div with 0)
            checkAssert(error);
        }
    });

    it("should create a new order", async () => {
        await token.addType("brilliant", 100);
        let createAnOrder = await token.createOrder("brilliant", "metadata", {value:1000});
        let expirationDate = parseInt(createAnOrder.logs[0].args.expirationDate);

        assert.equal(createAnOrder.logs[0].args.theType, "brilliant");
        assert.isAbove(expirationDate, parseInt(String(+new Date).slice(0,-3)));
        assert.equal(createAnOrder.logs[0].args.conversion.toNumber(), 10);
        assert.equal(createAnOrder.logs[0].args.metadata, "metadata");
    });

    it("should throw if you make a bid with not enough tokens", async () => {
        await token.addType("brilliant", 100);
        await token.createOrder("brilliant", "metadata", {value:1000});

        try {
            let makeABid = await token.bid(0, 10);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should throw if you make a bid to an expired order", async () => {
        let mockToken = await PoolBidTokenMock.new();
        await mockToken.addType("brilliant", 100);
        await mockToken.mint(accounts[0], 150);

        let mockDate = new Date();
        let timestamp = Math.floor(mockDate.subHours(1).getTime() / 1000);

        await mockToken.createMockOrder("brilliant", "metadata", timestamp, 0);

        try {
            let makeABid = await mockToken.bid(0, 10);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });


    it("should make a new bid", async () => {
        await token.addType("brilliant", 100);
        await token.createOrder("brilliant", "metadata");
        await token.mint(accounts[0], 50);
        let bid = await token.bid(0, 50);

        assert.equal(bid.logs[0].args.orderID.toNumber(), 0);
        assert.equal(bid.logs[0].args.amount.toNumber(), 50);
        assert.equal(bid.logs[0].args.overbid.toNumber(), 0);
        assert.equal(bid.logs[0].args.senderAddress, accounts[0]);
    });

    it("should allow overbid", async () => {
        await token.addType("brilliant", 10);
        await token.createOrder("brilliant", "metadata");
        await token.mint(accounts[0], 80);
        let bid = await token.bid(0, 80);
        let balance = await token.balanceOf(accounts[0]);

        // First event in receipt(index 0) is the OrderIsCompleted.
        // Second event is the BidSubmitted(log[1]).
        assert.equal(bid.logs[1].args.overbid.toNumber(), 70);
        assert.equal(balance, 70);
    });

    it("should throw if bid is closed", async () => {
        // Create a new mock order and fill it.
        await token.addType("brilliant", 100);
        await token.createOrder("brilliant", "metadata");
        await token.mint(accounts[0], 101);
        await token.bid(0, 100);

        try {
            let bidIsClosed = await token.bid(0, 1);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should throw if you try to withdraw more than your bid", async () => {
        await token.addType("brilliant", 100);
        await token.createOrder("brilliant", "metadata");
        await token.mint(accounts[0], 50);
        await token.bid(0, 50);

        try {
            let withdrawal = await token.withdraw(0, 51);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should throw if you try to withdraw from a closed bid", async () => {
        await token.addType("platelets", 50);
        await token.createOrder("platelets", "metadata");
        await token.mint(accounts[0], 150);
        await token.bid(0, 150);

        try {
            let withdrawal = await token.withdraw(0, 1);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should throw if you try to withdraw after the time restriction", async () => {
        let mockToken = await PoolBidTokenMock.new();
        await mockToken.addType("brilliant", 100);

        let mockDate = new Date();
        let timestamp = Math.floor(mockDate.addHours(1).getTime() / 1000);

        await mockToken.createMockOrder("brilliant", "metadata", timestamp, 80);

        try {
            let withdrawal = await mockToken.withdraw(0, 1);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should make a withdrawal successfully", async () => {
        await token.addType("platelets", 150);
        await token.createOrder("platelets", "metadata");
        await token.mint(accounts[0], 100);
        await token.bid(0, 100);

        let withdrawal = await token.withdraw(0, 50);
        assert.equal(withdrawal.logs[0].args.orderID.toNumber(), 0);
        assert.equal(withdrawal.logs[0].args.amount.toNumber(), 50);
        assert.equal(withdrawal.logs[0].args.senderAddress, accounts[0]);
    });

    it("should throw if if you try to claim token from a non closed order", async () => {
        let mockToken = await PoolBidTokenMock.new();
        await mockToken.addType("brilliant", 100);

        let mockDate = new Date();
        let timestamp = Math.floor(mockDate.addHours(1).getTime() / 1000);

        await mockToken.createMockOrder("brilliant", "metadata", timestamp, 80);

        try {
            let claimToken = await mockToken.claimOrderTokens(0);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should throw if if you try to claim token without having bid before", async () => {
        let mockToken = await PoolBidTokenMock.new();
        await mockToken.addType("platelets", 800);

        let mockDate = new Date();
        let timestamp = Math.floor(mockDate.getTime() / 1000);

        await mockToken.createMockOrder("platelets", "metadata", timestamp, 0);

        try {
            let claimToken = await mockToken.claimOrderTokens(0);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should throw if you try to claim token from a filled order", async () => {
        await token.addType("brilliant", 70);
        await token.createOrder("brilliant", "metadata");
        await token.mint(accounts[0], 100);
        await token.bid(0, 100);

        try {
            let claimToken = await token.claimOrderTokens(0);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should claim token from a filled order successfully", async () => {
        let mockToken = await PoolBidTokenMock.new();
        await mockToken.addType("platelets", 800);

        let mockDate = new Date();
        let timestamp = Math.floor(mockDate.getTime() / 1000);

        await mockToken.createMockOrder("platelets", "metadata", timestamp, 80);
        let claimToken = await mockToken.claimOrderTokens(0);
        assert.equal(claimToken.logs[0].args.orderID.toNumber(), 0);
        assert.equal(claimToken.logs[0].args.senderAddress, accounts[0]);
        assert.equal(claimToken.logs[0].args.claimedTokens.toNumber(), 80);
    });

    it("should throw if you try to claim ether from unfilled order", async () => {
        await token.addType("platelets", 150);
        await token.createOrder("platelets", "metadata");

        try {
            let makeEtherWithdrawal = await token.claimOrderEther(0);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

    it("should throw if you try to claim ether from an order you haven't bid", async () => {
        await token.addType("brilliant", 50);
        await token.createOrder("brilliant", "metadata");
        await token.mint(accounts[1], 100);
        await token.bid(0, 100, {from:accounts[1]});

        try {
            let makeEtherWithdrawal = await token.claimOrderEther(0);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });


    it("should claim ether from a filled bid correctly", async () => {
        await token.addType("brilliant", 50);
        // This will create a rate of 200/TMT.
        await token.createOrder("brilliant", "metadata", {value: 10000});
        await token.mint(accounts[0], 100);
        await token.bid(0, 100);

        let makeEtherWithdrawal = await token.claimOrderEther(0);
        assert.equal(makeEtherWithdrawal.logs[0].args.orderID.toNumber(), 0);
        assert.equal(makeEtherWithdrawal.logs[0].args.senderAddress, accounts[0]);
        assert.equal(makeEtherWithdrawal.logs[0].args.etherAmount.toNumber(), 10000);
    });

    it("should throw if you try to claim more ether from your bid amount", async () => {
        await token.addType("brilliant", 50);
        await token.createOrder("brilliant", "metadata");
        await token.mint(accounts[0], 100);
        await token.bid(0, 100);
        await token.claimOrderEther(0);

        try {
            let makeSecondWithrawal = await token.claimOrderEther(0);
            assert.fail("should have thrown");
        } catch (error) {
            checkRequire(error);
        }
    });

})
