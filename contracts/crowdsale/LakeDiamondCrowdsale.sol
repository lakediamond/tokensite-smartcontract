pragma solidity ^0.4.21;

import "./CappedCrowdsale.sol";
import "./RefundableCrowdsale.sol";
import "../token/TimeMachineToken.sol";
import "./Crowdsale.sol";
import "./TimedCrowdsale.sol";
import "./MintedCrowdsale.sol";
import "./WhitelistedCrowdsale.sol";

contract LakeDiamondCrowdsale is CappedCrowdsale, RefundableCrowdsale, MintedCrowdsale, WhitelistedCrowdsale {

  constructor(
    uint256 _openingTime,
    uint256 _closingTime,
    uint256 _rate,
    address _wallet,
    uint256 _cap,
    TimeMachineToken _token,
    uint256 _goal
  )
    public
    Crowdsale(_rate, _wallet, _token)
    CappedCrowdsale(_cap)
    TimedCrowdsale(_openingTime, _closingTime)
    RefundableCrowdsale(_goal)
  {
    //As goal needs to be met for a successful crowdsale
    //the value needs to less or equal than a cap which is limit for accepted funds
    require(_goal <= _cap);
  }
}
