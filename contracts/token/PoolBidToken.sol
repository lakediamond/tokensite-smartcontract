pragma solidity ^0.4.21;

import "./MintableToken.sol";

contract PoolBidToken is MintableToken {

    mapping (string => uint256) diamondType;
    mapping (uint256 => Order) public orders;
    uint256 public orderCounter;

    constructor() public {
        orderCounter = 0;
    }

    struct Order {
        uint256 id;
        uint256 target;
        uint256 currentTokenCount;
        uint expirationDate;
        uint256 tokenEthRatio;
        string metadata;
        mapping (address => uint256) investors;
        address[] investorAddresses;
    }

    modifier isStillOpen(uint256 orderID) {
        require(orders[orderID].expirationDate > now);
        require(orders[orderID].currentTokenCount > 0);
        _;
    }

    modifier isClosed(uint256 orderID) {
        require(orders[orderID].expirationDate <= now);
        require(orders[orderID].currentTokenCount > 0);
        _;
    }

    modifier isFilled(uint256 orderID) {
        require(orders[orderID].currentTokenCount == 0);
        _;
    }

    modifier canWithdraw(uint256 orderID) {
        require(orders[orderID].expirationDate > now + 6 hours);
        _;
    }

    function addType(string newType, uint256 targetTokens) onlyOwner public {
        diamondType[newType] = targetTokens;
    }

    function getTokenTarget(string theType) public view returns (uint256) {
        return diamondType[theType];
    }

    function createOrder(string theType, string metadata) onlyOwner public payable {
        // Assumption: msg.value has exactly the amount of ETH needed
        // TODO: ratio should be based on TMT and ether curent price
        address[] memory buffer;
        Order memory order = Order(
            orderCounter,
            getTokenTarget(theType),
            getTokenTarget(theType),
            now + 1 days,
            msg.value.div(getTokenTarget(theType)),
            metadata,
            buffer
        );
        orders[orderCounter] = order;
        emit OrderCreated(orderCounter, theType, now + 1 days, msg.value.div(getTokenTarget(theType)), metadata, now);

        orderCounter += 1;
    }

    function bid(uint256 orderID, uint256 tokenAmount) isStillOpen(orderID) public {
        require(balances[msg.sender] >= tokenAmount);

        uint256 overbid;
        if(orders[orderID].currentTokenCount < tokenAmount) {
            overbid = tokenAmount - orders[orderID].currentTokenCount;
            tokenAmount = orders[orderID].currentTokenCount;
        }

        // Update smart contract state.
        balances[msg.sender] = balances[msg.sender].sub(tokenAmount);
        totalSupply_ = totalSupply_.sub(tokenAmount);

        // Update order state.
        orders[orderID].currentTokenCount = orders[orderID].currentTokenCount.sub(tokenAmount);
        orders[orderID].investors[msg.sender] = orders[orderID].investors[msg.sender].add(tokenAmount);
        
        bool isExist = false;
        for (uint i = 0; i < orders[orderID].investorAddresses.length; i++) {
            if (orders[orderID].investorAddresses[i] == msg.sender){
               isExist = true; 
               break;
            }
        }
        if (!isExist) {
            orders[orderID].investorAddresses.push(msg.sender);
        }
        
        if(orders[orderID].currentTokenCount == 0) {
            sendOrderEther(orderID);
            emit OrderIsComplete(orderID, msg.sender, now);
        }
        emit BidSubmitted(orderID, tokenAmount, overbid, msg.sender, now);
    }

    function withdraw(uint256 orderID, uint256 amount) isStillOpen(orderID) canWithdraw(orderID) public {
        require(orders[orderID].investors[msg.sender] >= amount);

        // Update smart contract state.
        balances[msg.sender] = balances[msg.sender].add(amount);
        totalSupply_ = totalSupply_.add(amount);

        // Update order state.
        orders[orderID].investors[msg.sender] = orders[orderID].investors[msg.sender].sub(amount);
        orders[orderID].currentTokenCount = orders[orderID].currentTokenCount.add(amount);
        
        emit BidWithdrawal(orderID, amount, msg.sender, now);
    }

    function claimOrderTokens(uint256 orderID) isClosed(orderID) public {
        require(orders[orderID].investors[msg.sender] > 0);
        uint256 amount = orders[orderID].investors[msg.sender];

        balances[msg.sender] = balances[msg.sender].add(orders[orderID].investors[msg.sender]);
        totalSupply_ = totalSupply_.add(orders[orderID].investors[msg.sender]);
        orders[orderID].investors[msg.sender] = 0;
        emit TokensClaimed(orderID, msg.sender, amount, now);
    }

    function claimOrderEther(uint256 orderID) isFilled(orderID) public {
        require(orders[orderID].investors[msg.sender] > 0);

        uint256 amount = orders[orderID].investors[msg.sender].mul(orders[orderID].tokenEthRatio);
        orders[orderID].investors[msg.sender] = 0;
        msg.sender.transfer(amount);
        emit EtherClaimed(orderID, msg.sender, amount, now);
    }
    
    function sendOrderEther(uint256 orderID) internal {
        uint256 amount;
        for (uint i = 0; i < orders[orderID].investorAddresses.length; i++) {
            if (orders[orderID].investors[orders[orderID].investorAddresses[i]] != 0) {
                amount = orders[orderID].investors[orders[orderID].investorAddresses[i]].mul(orders[orderID].tokenEthRatio);
                orders[orderID].investors[orders[orderID].investorAddresses[i]] = 0;
                orders[orderID].investorAddresses[i].transfer(amount);
            }
        }
    }

    event OrderCreated(uint256 orderCounter, string theType, uint256 expirationDate, uint256 conversion, string metadata, uint timestamp);
    event OrderIsComplete(uint256 orderID, address senderAddress, uint timestamp);
    event BidSubmitted(uint256 orderID, uint256 amount, uint256 overbid, address senderAddress, uint timestamp);
    event BidWithdrawal(uint256 orderID, uint256 amount, address senderAddress, uint timestamp);
    event TokensClaimed(uint256 orderID, address senderAddress, uint256 claimedTokens, uint timestamp);
    event EtherClaimed(uint256 orderID, address senderAddress, uint256 etherAmount, uint timestamp);
}
