pragma solidity ^0.4.21;


import "./PoolBidToken.sol";


contract TimeMachineToken is PoolBidToken {

    string public constant name = "LakeDiamond Token"; // solium-disable-line uppercase
    string public constant symbol = "LKD"; // solium-disable-line uppercase

    uint256 public constant INITIAL_SUPPLY = 141120000;

    event EtherReceived(address _sender, uint256 _amount);
    /**
    * @dev Constructor that gives msg.sender all of existing tokens.
    */
    constructor() public {
        totalSupply_ = INITIAL_SUPPLY;
        balances[msg.sender] = INITIAL_SUPPLY;
        emit Transfer(0x0, msg.sender, INITIAL_SUPPLY);
    }

    function buyTMT(uint256 amount) public returns (bool) {
        balances[owner] = balances[owner].sub(amount);
        balances[msg.sender] = balances[msg.sender].add(amount);
        emit Transfer(owner, msg.sender, amount);
        return true;
    }

    /**
    * Only owner can send ether to smart contract.
    */
    function () onlyOwner public payable {
        emit EtherReceived(msg.sender, msg.value);
    }

}
